package com.binar.challenge2.service;

import com.binar.challenge2.exception.ReadWriteFileException;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ReadFileImpl extends ReadFile {

    @Override
    public List<Integer> readFile() {

        try {
            File file = new File(PATH_FILE_READ);
            if(!file.exists()){
                throw new ReadWriteFileException("Filenya tidak ada bestie.");
            }
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String line;
            List<Integer> listNilai = new ArrayList<>();

            while((line = br.readLine()) != null){
                String[] values = line.split(";");
                for (int i = 0; i < values.length; i++)
                    if (i != 0) {
                        String dataTemp = values[i];
                        int temp = Integer.parseInt(dataTemp);
//                        System.out.print(" " + temp);
                        listNilai.add(temp);
                    }
//                System.out.println();
            }
            br.close();
            return listNilai;
        } catch (IOException ioException){
            ioException.printStackTrace();
        }
        return null;
    }
}
